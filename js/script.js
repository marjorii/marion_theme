const html = document.querySelector('html');
const menuButton = document.querySelector('button#mobile img');

// Toggle mobile menu
document.querySelector('button#mobile').onclick = function() {
    document.querySelector('#list-menu').classList.toggle('hide');
    document.firstElementChild.classList.toggle('hide-overflow');
    document.getElementById('menu').classList.toggle('expanded');
    let src = menuButton.getAttribute('src');
    if (!document.querySelector('#list-menu').classList.contains('hide')) {
        src = src.replace('menu', 'cross');
    }
    else {
        src = src.replace('cross', 'menu');
    }
    menuButton.setAttribute('src', src);

    if (document.querySelector('#list-menu').classList.contains('hide')) {
        this.setAttribute('aria-expanded', 'false');
    }
    else {
        this.setAttribute('aria-expanded', 'true');
    }
}