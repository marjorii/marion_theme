const isTouchDevice = "ontouchstart" in window || navigator.msMaxTouchPoints;
const videos = Array.from(document.querySelectorAll('video'));
const elemFocus = document.getElementsByTagName('*');
const focusArray = [];
let prevElem = undefined;
let nextElem = undefined;

for (var i = 0, len = elemFocus.length; i < len; i++) {
    if (elemFocus[i].tabIndex >= '0' && window.getComputedStyle(elemFocus[i], null).display !== 'none') {
        focusArray.push(elemFocus[i]);
    }
};

videos.forEach((video) => {

    video.removeAttribute('controls');
    if (!isTouchDevice) {    
        video.addEventListener('mouseover', () => {
            video.setAttribute('controls', '');
        });
           
        video.addEventListener('mouseout', () => {
            video.removeAttribute('controls');
        });
    
        video.addEventListener('focus', () => {
            video.setAttribute('controls', '');
            focusArray.forEach((elem) => {
                if (video == elem) {
                    prevElem = focusArray[focusArray.indexOf(elem) - 1];
                    nextElem = focusArray[focusArray.indexOf(elem) + 1];
                }
            });
        });
        
        video.addEventListener('blur', () => {
            [prevElem, nextElem].forEach((elem) => {
                elem.addEventListener('focus', ()=> {
                    video.removeAttribute('controls');
                });
            })
        });
        
    }
    else {
        video.parentElement.addEventListener('click', videoContainerClick);
    }
    
    function videoContainerClick() {
        if (!video.getAttribute('controls')) {
            video.setAttribute('controls', '');
            video.parentElement.removeEventListener('click', videoContainerClick);
        }
    }
});
