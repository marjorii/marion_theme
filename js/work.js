const buttons = document.querySelectorAll('.project-container a');

function getNextImageIndex(img, filenames) {
    const filenamePath = img.src.substring(0, img.src.lastIndexOf("/") + 1);
    const filenameImg = (img.src.substring(img.src.lastIndexOf("/"), img.src.length)).replace('/', '');
    let imgIndex = filenames.indexOf(filenameImg);
    if (imgIndex === filenames.length - 1) {
        imgIndex = 0
    } else {
        imgIndex++
    }
    return filenamePath + filenames[imgIndex];
}

buttons.forEach((button) => {
    let i = 0;
    let originalSrc = button.querySelector('img.absolute').getAttribute('src');
    const originalSrcFilename = (originalSrc.substring(originalSrc.lastIndexOf("/"), originalSrc.length)).replace('/', '');
    const filenames = [originalSrcFilename, ...button.lastElementChild.dataset.filenames.split(', ')];
    if (window.innerWidth <= 360) {
        const nextImgSrc = getNextImageIndex(button.querySelector('img.absolute'), filenames);
        button.querySelector('img:not(.absolute)').src = nextImgSrc
    }

    let startPos = {clientX: null, clientY: null}
    
    function changeImage (e) {
        const { clientX, clientY } = e.touches[0]
        const diff = { x: clientX - startPos.clientX, y: clientY - startPos.clientY}

        startPos = {clientX: null, clientY: null}

        button.removeEventListener('touchmove', changeImage);
        setTimeout(() => {
            button.addEventListener('touchmove', changeImage);
        }, 300)

        if (diff.x > diff.y) return
        let unabsoluteImg = button.querySelector('img:not(.absolute)');
        let absoluteImg = button.querySelector('img.absolute');
        absoluteImg.classList.remove('absolute');
        unabsoluteImg.classList.add('absolute');

        const nextImgSrc = getNextImageIndex(unabsoluteImg, filenames);
        absoluteImg.src = nextImgSrc;
    }

    button.addEventListener('touchstart', (e) => {
        const { clientX, clientY } = e.touches[0]
        startPos = {clientX, clientY}
    });
    button.addEventListener('touchmove', changeImage)
});