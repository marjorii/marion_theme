/* Source: https://codepen.io/nachitz/pen/LXmGzN */

function resizeHomeImg() {
    document.querySelector('#img-container').style.height = window.innerHeight - document.querySelector('header').offsetHeight + 'px';
}

resizeHomeImg()
window.addEventListener('resize', resizeHomeImg);

function scrollHorizontally(e) {
    let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    const scrollSpeed = 60;
    document.querySelector('main').scrollLeft -= (delta * scrollSpeed);
    e.preventDefault();
}

// IE9, Chrome, Safari, Opera
document.querySelector('main').addEventListener("mousewheel", scrollHorizontally, false);
// Firefox
document.querySelector('main').addEventListener("DOMMouseScroll", scrollHorizontally, false);
