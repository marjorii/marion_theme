let container = document.getElementById('images-container');
let content = document.getElementById('content');
const toggleButton = document.querySelector('#teaching button#plus');

window.onload = () => {
    resizeContainer(container);
}

window.onresize = () => {
    resizeContainer(container);
}

groupListItems();

toggleButton.onclick = () => {
    const div = document.getElementById('hidden-list');
    if (div.classList.contains('hide')) {
        toggleButton.setAttribute('aria-expanded', 'true');
        div.classList.remove('hide');
        toggleButton.textContent = '-';
        document.body.classList.add('full-content');
    }
    else {
        toggleButton.setAttribute('aria-expanded', 'false');
        div.classList.add('hide');
        toggleButton.textContent = '+';
        document.body.classList.remove('full-content');
    }
}

function groupListItems() {
    const ul = toggleButton.parentElement.parentElement;
    const hiddenList = Array.from(document.querySelectorAll('#teaching li[class="hidden-item"]'));
    const div = document.createElement('div');
    div.setAttribute('id', 'hidden-list');
    div.setAttribute('class', 'hide');
    ul.appendChild(div);
    hiddenList.forEach((item) => {
        div.appendChild(item);
    });
}

function resizeContainer(container) { 
    if (window.innerWidth > 1023 && window.innerHeight > 799) {
        let contentHeight = container.offsetHeight + content.offsetHeight;
        const menuHeight = document.getElementById('menu').offsetHeight;
        if (contentHeight > window.innerHeight) {
            let newHeight = (window.innerHeight - content.offsetHeight) - menuHeight - 10;
            container.style.setProperty('height', newHeight + 'px');
        }
    }
    else {
        container.style.setProperty('height', 'unset');
    }
}

const aboutButtons = Array.from(document.querySelectorAll('button.mobile'));

aboutButtons.forEach((button) => {
    const cont = document.getElementById(button.getAttribute('aria-controls'));
    button.addEventListener('click', () => {
        const conts = Array.from(document.querySelectorAll('h2 + *'));
        const visibleElem = conts.find((elem) => !elem.classList.contains('hide'));
        if (cont.classList.contains('hide')) {
            if (visibleElem) {
                visibleElem.classList.add('hide');
                visibleElem.parentElement.querySelector('button.mobile').setAttribute('aria-expanded', 'false');
            }
            cont.classList.remove('hide');
            button.setAttribute('aria-expanded', 'true');
        }
        else {
            cont.classList.add('hide');
            button.setAttribute('aria-expanded', 'false');
        }
    });
});
  

